const expect = require('chai').expect;
const { SquarePower } = require('../src/classes/SquarePower');

describe('Testing the square power function', function() {
    it("1. basic use", function(done){
        let calc1 = new SquarePower(5);
        expect(calc1.getResult()).to.equal(25);
        done();
    });

    it("2. with zero", function(done){
        let calc2 = new SquarePower(0);
        expect(calc2.getResult()).to.equal(0);
        done();
    });

    it("3. with negatives", function(done){
        let calc3 = new SquarePower(-5);
        expect(calc3.getResult()).to.equal(25);
        done();
    });

    it("4. with decimals", function(done){
        let calc4 = new SquarePower(0.5);
        expect(calc4.getResult()).to.equal(0.25);
        done();
    });
});