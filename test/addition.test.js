const expect = require("chai").expect;
const { Addition } = require("../src/classes/Addition");

describe("Testing the additionate method", function () {
  it("1. Testing 2+2", function (done) {
    let operation = new Addition(2, 2);
    let operation2 = new Addition(2, 3);
    expect(operation.additionate()).to.equal(4);
    expect(operation2.additionate()).to.equal(5);
    done();
  });
});




