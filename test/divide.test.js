const expect = require('chai').expect;
const { Divide } = require("../src/classes/Divide");

describe('Testing the divide function', function() {
    it("1. basic use", function(done){
        let calc1 = new Divide(4,2);
        expect(calc1.getResult()).to.equal(2);
        let calc1_1 = new Divide(3,2);
        expect(calc1_1.getResult()).to.equal(1.5);
        done();
    });
  
    it("2. with zero", function(done){
        let calc2 = new Divide(4,0);
        expect(calc2.getResult()).to.equal("Impossible de diviser par zero");
        let calc2_1 = new Divide(0,4);
        expect(calc2_1.getResult()).to.equal(0); //will not lead to an error
        done();
    });
  
    it("3. with negatives", function(done){
        let calc3 = new Divide(4,-2);
        expect(calc3.getResult()).to.equal(-2);
        let calc3_1 = new Divide(-2,4);
        expect(calc3_1.getResult()).to.equal(-0.5);
        done();
    });
  
    it("4. with decimals", function(done){
        let calc4 = new Divide(2,0.5);
        expect(calc4.getResult()).to.equal(4);
        let calc4_1 = new Divide(0.5,2);
        expect(calc4_1.getResult()).to.equal(0.25);
        done();
    });
  });