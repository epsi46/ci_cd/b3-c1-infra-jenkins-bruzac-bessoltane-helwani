const expect = require('chai').expect;
const { SquareRoot } = require('../src/classes/SquareRoot');

describe('Testing the square root function', function() {
    it("1. basic use", function(done){
        let calc1 = new SquareRoot(25);
        expect(calc1.getResult()).to.equal(5);
        done();
    });

    it("2. with zero", function(done){
        let calc2 = new SquareRoot(0);
        expect(calc2.getResult()).to.equal(0);
        done();
    });

    it("3. with negatives", function(done){
        let calc3 = new SquareRoot(-5);
        expect(calc3.getResult()).to.equal("You can't found the square root of a negative number");
        done();
    });

    it("4. with decimals", function(done){
        let calc4 = new SquareRoot(30.25);
        expect(calc4.getResult()).to.equal(5.5);
        done();
    });
});