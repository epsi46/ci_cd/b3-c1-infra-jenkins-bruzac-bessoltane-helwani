const expect = require('chai').expect;
const { Power } = require('../src/classes/Power');

describe('Testing the power function', function() {
    it("1. basic use", function(done){
        let calc1 = new Power(5,2);
        expect(calc1.getResult()).to.equal(25);
        let calc1_1 = new Power(2,5);
        expect(calc1_1.getResult()).to.equal(32);
        done();
    });

    it("2. with zero", function(done){
        let calc2 = new Power(4,0);
        expect(calc2.getResult()).to.equal(1);
        let calc2_1 = new Power(0,4);
        expect(calc2_1.getResult()).to.equal(0);
        done();
    });

    it("3. with negatives", function(done){
        let calc3 = new Power(4,-2);
        expect(calc3.getResult()).to.equal("Une puissance ne peut pas être négative");
        let calc3_1 = new Power(-2,4);
        expect(calc3_1.getResult()).to.equal(16);
        let calc3_2 = new Power(-2,5);
        expect(calc3_2.getResult()).to.equal(-32);
        done();
    });

    it("4. with decimals", function(done){
        let calc4 = new Power(2,0.5);
        expect(calc4.getResult()).to.equal(1.41);
        let calc4_1 = new Power(0.5,2);
        expect(calc4_1.getResult()).to.equal(0.25);
        done();
    });
});