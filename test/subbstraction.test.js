const expect = require("chai").expect;
const { Subbstraction } = require("../src/classes/Subbstraction");

describe("Testing the subbstraction method", function () {
  it("1. Testing 2-2", function (done) {
    let operation = new Subbstraction(2, 2);
    expect(operation.subbstract()).to.equal(0);
    done();
  });
});




