
/* Feature: power calculation 

  Scenario: calculating the power of a number
    Given the value 4 to the power of 2
    When I calculate its power
    Then the result should be 16
*/

// Calculating power of numbers
class Power{
    constructor(oldvalor,newvalor) {
    this.oldvalor = oldvalor;
    this.newvalor = newvalor;
    }

    getResult () {
        if (this.newvalor >= 0) {
            let waitingvalor = this.newvalor;
            let newvalor = Math.round((this.oldvalor ** this.newvalor) * 100) / 100;
            this.oldvalor = waitingvalor;
            return newvalor
        }
        return "Une puissance ne peut pas être négative"
    }
}

module.exports = {
    Power: Power,
};