/* Feature: addition calculation 

  Scenario: calculating the addition of 2 numbers
    Given the value 50 and the other 50
    When I do the addition
    Then the result should be 100
*/

// Calculate addition of 2 numbers
class Addition {

    constructor(a,b) {
        this.a = a;
        this.b = b;
    }

    additionate () {
        return this.a+this.b;
    }
}

module.exports = {
    Addition: Addition,
};