/* Feature: divide calculation 

  Scenario: calculating the division between 2 numbers
    Given the value 50 and dividing it by 2
    When I calculate it
    Then the result should be 25
*/

// Calculation the result of a division between 2 numbers
class Divide{
    constructor(oldvalor,newvalor) {
    this.oldvalor = oldvalor;
    this.newvalor = newvalor;
    }

    getResult () {
        if (this.newvalor != 0) {
            let waitingvalor = this.newvalor;
            let newvalor = this.oldvalor / this.newvalor;
            this.oldvalor = waitingvalor;
            return newvalor
        }
        return "Impossible de diviser par zero"
    }
}

module.exports = {
    Divide: Divide,
};