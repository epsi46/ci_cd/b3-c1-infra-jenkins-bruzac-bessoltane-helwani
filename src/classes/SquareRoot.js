
/* Feature: square root calculation 

  Scenario: calculating the square root of a number
    Given the value 4 and finding its square root
    When I calculate it
    Then the result should be 2
*/

// Calculating square root of numbers
class SquareRoot{
    constructor(newvalor) {
    this.newvalor = newvalor;
    }

    getResult () {
        if (this.newvalor < 0) {
            return "You can't found the square root of a negative number";
        } else {
            let newvalor = Math.sqrt(this.newvalor);
            return newvalor
        }
    }
}

module.exports = {
    SquareRoot: SquareRoot,
};