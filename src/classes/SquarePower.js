
/* Feature: Square power calculation 

  Scenario: calculating the square power of a number
    Given the value 50 and its square power of 2
    When I calculate it
    Then the result should be 2500
*/

// Calculating square power of numbers
class SquarePower{
    constructor(newvalor) {
    this.newvalor = newvalor;
    }

    getResult () {
        let newvalor = this.newvalor ** 2;
        return newvalor
    }
}

module.exports = {
    SquarePower: SquarePower,
};