/* Feature: percentage calculation 

  Scenario: calculating the percentage of a number
    Given the value 50 and total 100
    When I calculate the percentage
    Then the result should be 50
*/

// Convert a number to a percentage
class PercentageCalculating {
  constructor(value, total) {
    this.value = value;
    this.total = total;
  }
  getPercentage() {
    if (this.total === 0) {
      return "Le total ne peut être zéro";
    }
    return (this.value / this.total) * 100;
  }
}

module.exports = {
  PercentageCalculating: PercentageCalculating,
};
